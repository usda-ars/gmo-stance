This is a digital repository of the data associated with "Analysis of biased language in peer-reviewed scientific literature on genetically modified crops"
Stevens, Bo Maxwell, Randi Reppen, Mark Linhart, Kara Gibson, Adrah Parafiniuk, Aradhana Roberts, Robert Sanford, and Nancy Collins Johnson. "Analysis of biased language in peer-reviewed scientific literature on genetically modified crops." Environmental Research Letters (2021).

https://iopscience.iop.org/article/10.1088/1748-9326/ac1467

All PDFs associated with this analyis can be found on Google Drive:
https://drive.google.com/drive/folders/1urwKXDrKEBbdi9Ad_AO5C0x4UwLLLRNE?usp=sharing
